package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeConv extends NodeExpr{
	private NodeExpr expr;
	private LangType type;
	
	public NodeConv(NodeExpr expr) {
		this.expr = expr;
		type = LangType.FLOAT;
	}

	@Override
	public String toString() {
		return "[CONV: " + expr.toString() + "]";
	}
	
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

	@Override
	public LangType getType() {
		return type;
	}

	public NodeExpr getExpr() {
		return expr;
	}


}
