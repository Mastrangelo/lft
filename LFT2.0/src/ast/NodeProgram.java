package ast;

import java.util.ArrayList;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;
import visitor.TypeVisitor;

public class NodeProgram extends NodeAst {
	private ArrayList<NodeDecl> nodeDecl;
	private ArrayList<NodeStm> nodeStm;
	
	public NodeProgram(ArrayList<NodeDecl> nodeDecl, ArrayList<NodeStm> nodeStm) {
		this.nodeDecl = nodeDecl;
		this.nodeStm = nodeStm;
	}
	

	public ArrayList<NodeDecl> getNodeDecl() {
		return nodeDecl;
	}


	public ArrayList<NodeStm> getNodeStm() {
		return nodeStm;
	}


	@Override
	public String toString() {
		return nodeDecl.toString() + nodeStm.toString();
	}

	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}
}
