package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public abstract class NodeStm extends NodeAst {
	
	public abstract void accept(AbsVisitor visitor) throws DuplicateDeclException;

}
