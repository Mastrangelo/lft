package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeAssign extends NodeStm {
	private NodeId id;
	private NodeExpr expr;
	
	public NodeAssign(NodeId id, NodeExpr expr) {
		this.id = id;
		this.expr = expr;
	}
	
	public NodeId getId() {
		return id;
	}

	public NodeExpr getExpr() {
		return expr;
	}

	public void setExpr(NodeExpr expr) {
		this.expr = expr;
	}

	@Override
	public String toString() {
		return id.toString() + " = " + expr.toString();
	}

	@Override
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

}
