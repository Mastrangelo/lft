package ast;

public class NodeId extends NodeAst {
	private String name;
	
	public NodeId(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
