package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeDecl extends NodeAst {
	private NodeId id;
	private LangType type;

	public NodeDecl(NodeId id, LangType type) {
		this.id = id;
		this.type = type;
	}

	public NodeId getId() {
		return id;
	}

	public LangType getType() {
		return type;
	}

	@Override
	public String toString() {
		return (type == LangType.FLOAT)?  ("f " + id) : ("i " + id);
	}
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}
}
