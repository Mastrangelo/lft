package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public abstract class NodeExpr extends NodeAst {
	
	public abstract LangType getType();

	public abstract void accept(AbsVisitor visitor) throws DuplicateDeclException;
	
}
