package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeCost extends NodeExpr {
	private String value;
	private LangType type;
	
	public NodeCost(String value, LangType type) {
		this.value = value;
		this.type = type;
	}

	@Override
	public String toString() {
		return (type == LangType.FLOAT)? ("f " + value) : ("i " + value);
	}

	@Override
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

	@Override
	public LangType getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
	

}
