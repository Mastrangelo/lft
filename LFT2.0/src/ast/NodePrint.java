package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodePrint extends NodeStm {
	private NodeId id;
	
	public NodePrint(NodeId id) {
		this.id = id;
	}

	public NodeId getId() {
		return id;
	}

	@Override
	public String toString() {
		return "p " + id;
	}
	
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

}
