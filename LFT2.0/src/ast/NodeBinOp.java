package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeBinOp extends NodeExpr {
	private LangOper op;
	private NodeExpr left;
	private NodeExpr right;

	
	public NodeBinOp(LangOper op, NodeExpr left, NodeExpr right) {
		this.op = op;
		this.left = left;
		this.right = right;
	}

	public String getOp() {
		return op==LangOper.PLUS? "+":"-";
	}

	public NodeExpr getLeft() {
		return left;
	}

	public NodeExpr getRight() {
		return right;
	}
	
	public void setLeft(NodeExpr left) {
		this.left = left;
	}

	public void setRight(NodeExpr right) {
		this.right = right;
	}

	@Override
	public LangType getType() {
		return left.getType();
	}

	@Override
	public String toString() {
		return (op == LangOper.PLUS)? (left + " + " + right) : (left + " - " + right);
	}

	@Override
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

	

}
