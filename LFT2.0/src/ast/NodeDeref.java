package ast;

import visitor.AbsVisitor;
import visitor.DuplicateDeclException;

public class NodeDeref extends NodeExpr {
	private NodeId id;
	private LangType type;
	
	public NodeDeref(NodeId id) {
		this.id = id;
	}
	
	public NodeId getId() {
		return id;
	}

	public void setType(LangType type) {
		this.type = type;
	}

	@Override
	public LangType getType() {
		return type;
	}


	@Override
	public String toString() {
		return id.toString();
	}

	@Override
	public void accept(AbsVisitor visitor) throws DuplicateDeclException {
		visitor.visit(this);
	}

	
	

}
