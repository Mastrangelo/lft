package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import ast.NodeProgram;
import parser.Parser;
import scanner.Scanner;
import visitor.CodeGeneratorVisitor;
import visitor.TypeVisitor;

/**
 * Classe Main che genera il codice dc dato in input 
 * un file in codice ac
 * 
 * @author Simone Mastrangelo
 *
 */
public class Main {

	/**
	 * Metodo main della classe, richiede all'utente il nome
	 * del file del programma ac da tradurre 
	 * ed il nome del file dc da creare.
	 * 
	 * Ex. "fileAc.txt" cerca nella root del programma un file di questo nome 
	 * EX. "fileDc.txt" genera il file dentro la root del programma
	 * 
	 * @throws Excepti1on
	 */
	public static void main(String[] args) throws Exception {	
		InputStreamReader reader = new InputStreamReader(System.in);
		BufferedReader myInput = new BufferedReader(reader);
		String inputFile= new String();
		String outputFile= new String();
		CodeGeneratorVisitor codeGenerator = new CodeGeneratorVisitor();
		
		System.out.print("Inserire nome programma ac: ");
		inputFile = myInput.readLine();
	
		System.out.print("Inserire nome programma dc: ");
		outputFile = myInput.readLine();
		
		try {
			Parser parser = new Parser(new Scanner(inputFile));
			NodeProgram nodeProgram = parser.parse();
			nodeProgram.accept(new TypeVisitor());			
			nodeProgram.accept(codeGenerator);	
			codeGenerator.generateFile(outputFile);		
			System.out.println("Programma compilato correttamente");
		
		}catch (Exception exc) {
			System.out.println(exc);
			System.out.println("ERRORE: programma non compilato");
		}

	}

}
