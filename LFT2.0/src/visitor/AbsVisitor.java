package visitor;

import ast.*;

public abstract class AbsVisitor {
	
	public abstract void visit (NodeProgram n) throws DuplicateDeclException;
	
	public abstract void visit (NodeId n) throws DuplicateDeclException;
	
	public abstract void visit (NodeDecl n) throws DuplicateDeclException;
	
	public abstract void visit (NodePrint n) throws DuplicateDeclException;

	public abstract void visit (NodeAssign n) throws DuplicateDeclException;

	public abstract void visit (NodeCost n) throws DuplicateDeclException;

	public abstract void visit (NodeConv n) throws DuplicateDeclException;

	public abstract void visit (NodeDeref n) throws DuplicateDeclException;

	public abstract void visit (NodeBinOp n) throws DuplicateDeclException;


}