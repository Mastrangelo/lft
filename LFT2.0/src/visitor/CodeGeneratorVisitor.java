package visitor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ast.NodeAssign;
import ast.NodeBinOp;
import ast.NodeConv;
import ast.NodeCost;
import ast.NodeDecl;
import ast.NodeDeref;
import ast.NodeId;
import ast.NodePrint;
import ast.NodeProgram;
import ast.NodeStm;

/**
 * Classe che utilizzando il pattern visitor genera una stringa
 * contenete il programma tradotto in linguaggio dc.
 * Vengono visitati solo i NodeStm in quanto in dc non esistono dichiarazioni
 * 
 * @author Simone Mastrangelo
 *
 */
public class CodeGeneratorVisitor extends AbsVisitor{
	private StringBuffer stringBuff = new StringBuffer();
	
	
	@Override
	public String toString() {
		return stringBuff.toString();
	}

	/**
	 * Genera un file contenete il programma tradotto in dc.
	 * Il file deve essere in formato .txt o .dc (ex. myprg.txt)
	 * 
	 * @param fileName il nome del file
	 */
	public void generateFile(String fileName){
		try{
			File file= new File(fileName);
			FileWriter fw = new FileWriter(file);
			fw.append(stringBuff.toString());
			fw.append("\n");
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void visit(NodeProgram n) throws DuplicateDeclException {
		
		ArrayList<NodeStm> nodeStms = n.getNodeStm();
				
		if(nodeStms != null) {
		for (NodeStm nodeStm : nodeStms) 
			nodeStm.accept(this);}
	}

	@Override
	public void visit(NodeId n) throws DuplicateDeclException {
		// TODO Auto-generated method stub		
	}

	@Override
	public void visit(NodeDecl n) throws DuplicateDeclException {
		// TODO Auto-generated method stub	
	}

	@Override
	public void visit(NodePrint n) throws DuplicateDeclException {
		stringBuff.append("l" + n.getId() + " p si");
		
	}

	@Override
	public void visit(NodeAssign n) throws DuplicateDeclException {
		n.getExpr().accept(this);
		stringBuff.append("s" + n.getId() + " ");
		
	}

	@Override
	public void visit(NodeCost n) throws DuplicateDeclException {
		stringBuff.append(n.getValue() + " ");
		
	}

	@Override
	public void visit(NodeConv n) throws DuplicateDeclException {
		stringBuff.append("2 k ");
		n.getExpr().accept(this);
		
	}

	@Override
	public void visit(NodeDeref n) throws DuplicateDeclException {
		stringBuff.append("l" + n.getId() + " ");
		
	}

	@Override
	public void visit(NodeBinOp n) throws DuplicateDeclException {
		n.getLeft().accept(this);
		n.getRight().accept(this);
		stringBuff.append(n.getOp() + " ");
		
	}

}
