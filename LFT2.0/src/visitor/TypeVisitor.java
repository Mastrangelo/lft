package visitor;

import java.util.ArrayList;

import ast.NodeAssign;
import ast.NodeBinOp;
import ast.NodeConv;
import ast.NodeCost;
import ast.NodeDecl;
import ast.NodeDeref;
import ast.NodeExpr;
import ast.NodeId;
import ast.NodePrint;
import ast.NodeProgram;
import ast.NodeStm;
import symTable.STEntry;
import symTable.SymTable;


/**
 * Classe che utilizzando il pattern visitor si occupa di inserire i valori nella SymbolTable,
 * di controllare che la semantica dell'albero sia corretta, 
 * ed eventualmente decorare i nodi che necessitano una conversione di tipo (INT => FLOAT)
 * 
 * @author Simone Mastrangelo
 *
 */
public class TypeVisitor extends AbsVisitor {

	@Override
	public void visit(NodeProgram n) throws DuplicateDeclException {
		ArrayList<NodeDecl> nodeDecls = n.getNodeDecl();
		ArrayList<NodeStm> nodeStms = n.getNodeStm();
		
		for (NodeDecl nodeDecl : nodeDecls)
			nodeDecl.accept(this);
		
		if(nodeStms != null) {
		for (NodeStm nodeStm : nodeStms) 
			nodeStm.accept(this);
		}
		
	}

	@Override
	public void visit(NodeId n) throws DuplicateDeclException {
		STEntry st = SymTable.lookup(n.toString());
		if(st == null) throw new DuplicateDeclException();		
	}

	@Override
	public void visit(NodeDecl n) throws DuplicateDeclException  {
		if(! SymTable.enter(n.getId().toString(),new STEntry(n.getType()))) throw new DuplicateDeclException();
	}

	@Override
	public void visit(NodePrint n) throws DuplicateDeclException {
		this.visit(n.getId());
	}

	@Override
	public void visit(NodeAssign n) throws DuplicateDeclException {
		STEntry entry = SymTable.lookup(n.getId().toString());
		if(entry == null) throw new DuplicateDeclException();	
		n.getExpr().accept(this);
		n.setExpr(TypeCheckingUtil.convert(n.getExpr(), entry.getType()));
		
	}
 
	@Override
	public void visit(NodeCost n) { }

	@Override
	public void visit(NodeConv n) { }

	@Override
	public void visit(NodeDeref n) throws DuplicateDeclException {
		STEntry entry = SymTable.lookup(n.getId().toString());
		if(entry == null) throw new DuplicateDeclException();
		n.setType(entry.getType());
		
	}

	@Override
	public void visit(NodeBinOp n) throws DuplicateDeclException {
		NodeExpr leftExpr = n.getLeft();
		NodeExpr rightExpr = n.getRight();
		leftExpr.accept(this);
		rightExpr.accept(this);
		NodeExpr[] c = TypeCheckingUtil.consistent(leftExpr, rightExpr);
		n.setLeft(c[0]);
		n.setRight(c[1]);
		
	}

}
