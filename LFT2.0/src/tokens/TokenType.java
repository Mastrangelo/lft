package tokens;

public enum TokenType {

	FLOATDCL,
	INTDCL,
	ASSIGN,
	PRINT,
	ID,
	PLUS,
	MINUS,
	INUM,
	FNUM,
	SPACE,
	EOF
}
	

