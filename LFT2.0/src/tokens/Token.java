package tokens;

public class Token {
	private final TokenType type;
	private final String value;
		
		public Token(TokenType type, String value) {
			this.type = type;
			this.value = value;
		}
		
		public TokenType getType() {
			return type;
		}

		public String getValue() {
			return value;
		}
		
		@Override
		public String toString() {
			StringBuffer bf = new StringBuffer("<");
			bf.append(type);
			if(value != null)
				bf.append(", " + value);
			bf.append(">");
			
			return bf.toString();
			
		}
		
		
		
}
