package scanner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;

import tokens.LexicalException;
import tokens.Token;
import tokens.TokenType;

/**
 * La classe Scanner effettua l'analisi lessicale di un programma sorgente 
 * generandone gli appositi token, lancia eccezzione se fallisce.
 * 
 * @author Simone Mastrangelo
 *
 */
public class Scanner {
	
	private PushbackReader buffer;
	private Token lookaheadTk = null;
	
	/**
	 * Costruttore della classe
	 * 
	 * @param FileName il file in codice ac
	 * @throws FileNotFoundException
	 */
	public Scanner(String FileName) throws FileNotFoundException {
			this.buffer = new PushbackReader(new FileReader(FileName));
	}
	
	//Genera i token corrispondenti alla sintassi del file in ingresso, altrimenti lancia LexicalException
	private Token nextTokenWithRegex() throws IOException, LexicalException {
		
		StringBuffer stringBuffer = new StringBuffer();
		int currentChar = buffer.read();
		
		while(isSpace(currentChar)) {
			if(currentChar == -1)
				return new Token(TokenType.EOF, null);
			currentChar = buffer.read();
		}
		
		while(!isSpace(currentChar)) {
			stringBuffer.append((char) currentChar);
			currentChar = buffer.read();
		}
		
		String tokenToCheck = stringBuffer.toString();
		
		if(tokenToCheck.matches("[0-9]+"))								return new Token(TokenType.INUM, tokenToCheck);
		
		if(tokenToCheck.matches("[0-9]+.[0-9]+"))						return new Token(TokenType.FNUM, tokenToCheck);
		
		if(tokenToCheck.matches("f"))									return new Token(TokenType.FLOATDCL, null);
		
		if(tokenToCheck.matches("i"))									return new Token(TokenType.INTDCL, null);
		
		if(tokenToCheck.matches("p"))									return new Token(TokenType.PRINT, null);
		
		if(tokenToCheck.matches("([a-e]|[g-h]|[j-o]|[q-z])"))			return new Token(TokenType.ID, tokenToCheck);
		
		if(tokenToCheck.matches("\\+"))									return new Token(TokenType.PLUS, null);
		
		if(tokenToCheck.matches("\\-"))									return new Token(TokenType.MINUS, null);
		
		if(tokenToCheck.matches("="))									return new Token(TokenType.ASSIGN, null);
		
		throw new LexicalException("Token '" + tokenToCheck + "' non identificato");
	}
	
	//Ritorna true se il carattere dato come parametro in ingresso � uno spazio, false altrimenti
	private boolean isSpace(int c) {
		return (c == -1 || c == '\r' ||c == '\n' || c == ' ' || c == '\t') ? true : false;
	}
	
	/**
	 * Restituisce il token successivo
	 * 
	 * @return il Token successivo
	 * @throws IOException
	 * @throws LexicalException
	 */
	public Token nextToken() throws IOException, LexicalException {
		Token token = null;
		if(lookaheadTk == null) {
			token = nextTokenWithRegex();		
		}	
		else {
			token = lookaheadTk;
			lookaheadTk = null;
		}
		
		return token;
	}
	
	/**
	 * Restituisce il token corrente
	 * 
	 * @return il Token corrente
	 * @throws IOException
	 * @throws LexicalException
	 */
	public Token peekToken() throws IOException, LexicalException {
		if(lookaheadTk == null)
			lookaheadTk = nextToken();
		
		return lookaheadTk;
	}

}
