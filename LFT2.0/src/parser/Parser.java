package parser;

import java.io.IOException;
import java.util.ArrayList;

import ast.LangOper;
import ast.LangType;
import ast.NodeAssign;
import ast.NodeBinOp;
import ast.NodeCost;
import ast.NodeDecl;
import ast.NodeDeref;
import ast.NodeExpr;
import ast.NodeId;
import ast.NodePrint;
import ast.NodeProgram;
import ast.NodeStm;
import scanner.Scanner;
import symTable.STEntry;
import symTable.SymTable;
import tokens.LexicalException;
import tokens.Token;
import tokens.TokenType;

public class Parser {
	private Scanner scanner;
	private Token currentToken;
	
	public Parser(Scanner scanner) {		
		this.scanner = scanner;
	}
		
	private void nextToken() throws IOException, LexicalException {
		currentToken = scanner.nextToken();
	}
	
	private TokenType peekTokenType() throws IOException, LexicalException {
		currentToken = scanner.peekToken();
		return currentToken.getType();
	}
	
	public NodeProgram parse() throws Exception {
		SymTable.init();
		return parsPrg();
	}
	
	private NodeProgram parsPrg() throws Exception {
		ArrayList<NodeDecl> nodeDecl = new ArrayList<NodeDecl>();
		ArrayList<NodeStm> nodeStm = new ArrayList<NodeStm>();
		
		if(peekTokenType() == TokenType.FLOATDCL || peekTokenType() == TokenType.INTDCL)			
			nodeDecl = parseDcls();
					
		if(peekTokenType() == TokenType.ID || peekTokenType() == TokenType.PRINT)
			nodeStm = parseStms();
		
		if(peekTokenType() == TokenType.EOF)
			match(TokenType.EOF);	
		
		else
			throw new LexicalException(null);
		
		return new NodeProgram(nodeDecl, nodeStm);
	}
		
	private ArrayList<NodeDecl> parseDcls() throws Exception {	
		if(peekTokenType() == TokenType.FLOATDCL || peekTokenType() == TokenType.INTDCL) {
			NodeDecl node = parseDcl();	
			ArrayList<NodeDecl> nodeDeclList  = parseDcls();
			nodeDeclList.add(0, node);
			return nodeDeclList;
		}		
		return new ArrayList<NodeDecl>();
		
	}
	
	private NodeDecl parseDcl() throws Exception {
		if(peekTokenType() == TokenType.FLOATDCL) {
			match(TokenType.FLOATDCL);
			match(TokenType.ID);	
			return new NodeDecl(new NodeId(currentToken.getValue()), LangType.FLOAT);
			
		}
		else if(peekTokenType() == TokenType.INTDCL) {
			match(TokenType.INTDCL);		
			match(TokenType.ID);
			return new NodeDecl(new NodeId(currentToken.getValue()), LangType.INT);
		}
		else
			throw new LexicalException(null);

	}

	private ArrayList<NodeStm> parseStms() throws IOException, LexicalException {
		if(peekTokenType() == TokenType.ID || peekTokenType() == TokenType.PRINT) {
			NodeStm node = parseStm();
			ArrayList<NodeStm> nodeDeclList = parseStms();
			nodeDeclList.add(0, node);
			return nodeDeclList;
		}
		
		return new ArrayList<NodeStm>();
	}
	
	private NodeStm parseStm() throws IOException, LexicalException {
		if(peekTokenType() == TokenType.ID) {
			match(TokenType.ID);
			NodeId id = new NodeId(currentToken.getValue());
			match(TokenType.ASSIGN);			
			return new NodeAssign(id, parseExpr(parseVal()));
		}	
		
		if(peekTokenType() == TokenType.PRINT) { 
			match(TokenType.PRINT);
			match(TokenType.ID);
			return new NodePrint(new NodeId(currentToken.getValue()));
			
		}
		return null;
	}

	private NodeExpr parseVal() throws IOException, LexicalException {
		if(peekTokenType() == TokenType.INUM) {
			match(peekTokenType());
			return new NodeCost(currentToken.getValue(), LangType.INT);
		}
		if(peekTokenType() == TokenType.FNUM) {
			match(peekTokenType());
			return new NodeCost(currentToken.getValue(), LangType.FLOAT);
		}
		if(peekTokenType() == TokenType.ID) {
			match(TokenType.ID);
			return new NodeDeref(new NodeId(currentToken.getValue()));
		}
		else
			throw new LexicalException(null);
	}

	private NodeExpr parseExpr(NodeExpr expr) throws IOException, LexicalException {
		if(peekTokenType() == TokenType.PLUS) {
			match(TokenType.PLUS);
			return new NodeBinOp(LangOper.PLUS, expr, parseExpr(parseVal()));
		}
		if(peekTokenType() == TokenType.MINUS) {
			match(TokenType.MINUS);
			return new NodeBinOp(LangOper.MINUS, expr, parseExpr(parseVal()));
		}	
		
		return expr;
	}
	
	private void match(TokenType tokenType) throws IOException, LexicalException {		
		if(tokenType == peekTokenType()) {
					nextToken();
		}
		else
			throw new LexicalException(null);
	}
	
}
