package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;

import ast.NodeDecl;
import ast.NodeProgram;
import parser.Parser;
import scanner.Scanner;

public class TestNode {

	@Test
	public void test() throws Exception {
		Parser p = new Parser(new Scanner("testFile2.txt"));
		NodeProgram nodeProgram = p.parse();
		//assertEquals("[b, a, c, d]", nodeProgram.toString());		
		System.out.println(nodeProgram.toString());
	}

}
