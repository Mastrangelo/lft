package test;

import org.junit.Test;
import ast.NodeProgram;
import parser.Parser;
import scanner.Scanner;
import symTable.SymTable;
import visitor.CodeGeneratorVisitor;
import visitor.TypeVisitor;

public class TestVisitor {

	@Test
	public void test() throws Exception {
		Parser p = new Parser(new Scanner("testFile.txt"));
		NodeProgram nodeProgram = p.parse();
		nodeProgram.accept(new TypeVisitor());
		System.out.println(SymTable.toStr());
		System.out.println(nodeProgram.toString());
		
		CodeGeneratorVisitor cg = new CodeGeneratorVisitor();
		nodeProgram.accept(cg);
		System.out.println(cg.toString());
		
		cg.generateFile("prg.txt");
	}

}
