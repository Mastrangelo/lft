package test;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import scanner.Scanner;
import tokens.LexicalException;
import tokens.Token;
import tokens.TokenType;

public class TestScanner {
	
	@Test
	public void testNextTokenWithRegex() throws Exception {
		Scanner scnr = new Scanner("testFile3.txt");
		Token t = null;
		ArrayList<Token> ls = new ArrayList<Token>();
		try {
			t = scnr.nextTokenWithRegex();
			while(t.getType() != TokenType.EOF) {
				ls.add(t);
				t = scnr.nextTokenWithRegex();
			}
			ls.add(t);
		} catch (IOException | LexicalException e) {
			e.printStackTrace();
		}
		for (Token token : ls) {
			System.out.println(token.toString());
		}
	}
	
	public void testNextToken() throws Exception {
		Scanner scnr = new Scanner("testFile.txt");
		Token t = null;
		ArrayList<Token> ls = new ArrayList<Token>();
		t = scnr.nextToken();
		while(t.getType()!= TokenType.EOF) {
			ls.add(t);
			t = scnr.nextToken();
		}
		ls.add(t);
		for (Token token : ls) {
			System.out.println(token.toString());
		}
	}
}

