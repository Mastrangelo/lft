package test;

import static org.junit.Assert.*;

import org.junit.Test;

import tokens.Token;
import tokens.TokenType;

public class TestToken {

	@Test
	public void test() {
		Token t = new Token(TokenType.PRINT, "10");
		assertEquals("<PRINT, 10>", t.toString());
		t = new Token(TokenType.EOF, null);
		assertEquals("<EOF>", t.toString());
	}
	

}

